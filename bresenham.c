/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   bresenham.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rlegendr <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/12 09:40:10 by rlegendr     #+#   ##    ##    #+#       */
/*   Updated: 2018/11/22 11:57:13 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

void	ft_bre_init(t_bre *bre, t_params *par)
{
	bre->dx = ft_abs(par->x1 - par->x0);
	bre->dy = ft_abs(par->y1 - par->y0);
	bre->sx = par->x0 < par->x1 ? 1 : -1;
	bre->sy = par->y0 < par->y1 ? 1 : -1;
	bre->err = (bre->dx > bre->dy ? bre->dx : -(bre->dy)) / 2;
}

void	ft_bre_params_init(t_params *par, t_bre *bre)
{
	par->save_x = par->x0;
	par->save_y = par->y0;
	ft_bre_init(bre, par);
}

void	ft_check_color(t_fdf_params *data, int i, int j)
{
	if (ft_atoi(data->fdf[i][j]) != 0)
		ft_dec_to_rgb(data->end_color, data->rgb);
	else if (ft_atoi(data->fdf[i][j]) == 0 && data->fdf[i][j + 1] &&
			ft_atoi(data->fdf[i][j + 1]) != 0 && data->mv == 0)
		ft_dec_to_rgb(data->end_color, data->rgb);
	else if (ft_atoi(data->fdf[i][j]) == 0 && data->fdf[i + 1] &&
			ft_atoi(data->fdf[i + 1][j]) != 0 && data->mv == 1)
		ft_dec_to_rgb(data->end_color, data->rgb);
	else
		ft_dec_to_rgb(data->bre_color, data->rgb);
}

void	bresenham(t_fdf_params *data, t_params *par, int i, int j)
{
	t_bre			*bre;

	if ((bre = malloc(sizeof(t_bre))) == NULL)
		exit(0);
	ft_bre_params_init(par, bre);
	if (data->ok_end_color == 1)
		ft_check_color(data, i, j);
	while (1)
	{
		ft_modif_image(data, par->save_x, par->save_y);
		if (par->save_x == par->x1 && par->save_y == par->y1)
			break ;
		bre->e2 = bre->err;
		if (bre->e2 > -bre->dx)
		{
			bre->err -= bre->dy;
			par->save_x += bre->sx;
		}
		if (bre->e2 < bre->dy)
		{
			bre->err += bre->dx;
			par->save_y += bre->sy;
		}
	}
	free(bre);
}

void	bresenham_mouse(t_fdf_params *data)
{
	t_bre			*bre;

	if ((bre = malloc(sizeof(t_bre))) == NULL)
		exit(0);
	ft_bre_mouse_init(data, bre);
	while (1)
	{
		ft_modif_image(data, data->m_save_x, data->m_save_y);
		if (data->m_save_x == data->m_x1 && data->m_save_y == data->m_y1)
			break ;
		bre->e2 = bre->err;
		if (bre->e2 > -bre->dx)
		{
			bre->err -= bre->dy;
			data->m_save_x += bre->sx;
		}
		if (bre->e2 < bre->dy)
		{
			bre->err += bre->dx;
			data->m_save_y += bre->sy;
		}
	}
	free(bre);
}
