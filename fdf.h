/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   fdf.h                                            .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rlegendr <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/09 09:24:56 by rlegendr     #+#   ##    ##    #+#       */
/*   Updated: 2018/11/26 12:29:24 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include "libft/libft.h"
# include "minilibx_macos/mlx.h"

typedef struct	s_fdf_params
{
	int		scr_x;
	int		scr_y;
	int		begin;
	char	*filename;
	char	***fdf;
	void	*sync;
	void	*win;
	char	*str_image;
	void	*image;
	int		up;
	int		s_x;
	int		s_y;
	int		pos_x;
	int		pos_y;
	int		b;
	int		d;
	int		mv;
	int		back_color;
	int		band_color;
	int		shader_band_color;
	int		bre_color;
	int		end_color;
	int		ok_end_color;
	int		segment_type;
	int		color[10];
	int		back_col[10];
	char	rgb[3];
	int		m_x0;
	int		m_y0;
	int		m_x1;
	int		m_y1;
	int		m_save_x;
	int		m_save_y;

}				t_fdf_params;

typedef struct	s_params
{
	double	x0;
	double	y0;
	double	x1;
	double	y1;
	double	save_x;
	double	save_y;
}				t_params;

typedef struct	s_bre
{
	int dx;
	int dy;
	int sx;
	int sy;
	int err;
	int e2;
}				t_bre;

/*
** main.c
*/

char			***read_fdf(const int fd, char ***fdf);
int				error(int fd, int i);
int				ft_verif_tab_line(char ***fdf);
int				check_error_fdf(int argc, char *file, char ****fdf);

/*
** image.c
*/

void			ft_fill_pixel_band(t_fdf_params *data);
void			ft_fill_pixel_back(t_fdf_params *data);
void			ft_image(t_fdf_params *data);

/*
** string.c
*/

void			ft_instruct_string(t_fdf_params *data);
void			ft_color_string(t_fdf_params *data);
void			ft_back_string(t_fdf_params *data);
void			ft_smaller_res(t_fdf_params *data);
void			ft_bigger_res(t_fdf_params *data);

/*
** bresenham.c
*/

void			ft_bre_init(t_bre *bre, t_params *par);
void			ft_bre_params_init(t_params *par, t_bre *bre);
void			ft_check_color(t_fdf_params *data, int i, int j);
void			bresenham(t_fdf_params *da, t_params *par, int i, int j);
void			bresenham_mouse(t_fdf_params *data);

/*
** put_tab_on_screen.c
*/

void			calc_segment_init(t_params *par, int *dx, int *dy);
void			calc_segment(t_fdf_params *data, t_params *par, int i, int j);
void			ft_seg_type(t_fdf_params *data, t_params *par, int i, int j);
void			ft_print2(t_fdf_params *da, t_params *par, int i, int j);
void			ft_print_tab_on_screen(t_fdf_params *da);

/*
** keyboard.c
*/

void			ft_key1(t_fdf_params *data, int key);
void			ft_key2(t_fdf_params *data, int key);
void			ft_key3(t_fdf_params *data, int key);
int				ft_verif_key(int key, t_fdf_params *data);
int				ft_keyboard(int key, void *param);

/*
** mouse.c
*/

void			ft_bre_mouse_init(t_fdf_params *data, t_bre *bre);
void			ft_mouse_init(t_fdf_params *data);
void			ft_mouse_move(t_fdf_params *data);
int				ft_mouse(int button, int x, int y, t_fdf_params *data);

/*
** tool1.c
*/

void			ft_init_data(t_fdf_params *data);
void			ft_hex_to_rgb(char *hex_color, char *rgb);
char			*ft_dec_to_hex(char *hex_color, int color);
void			ft_modif_image(t_fdf_params *data, int x, int y);
void			ft_dec_to_rgb(int color, char *rgb);

/*
** tool2.c
*/

void			ft_init_color(int *color, int *back_col);
int				ft_change_color(int *color, int actual);
void			ft_print_triple_tab(char ***tab);
int				ft_scope_calc(int scope);
int				ft_zoom_init(char ***fdf);

#endif
