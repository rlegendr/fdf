/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   image.c                                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rlegendr <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/12 18:03:52 by rlegendr     #+#   ##    ##    #+#       */
/*   Updated: 2018/11/22 11:36:07 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

#include <stdio.h>

void	ft_fill_pixel_band(t_fdf_params *data)
{
	int		i;
	int		x;
	int		y;

	x = 0;
	y = -1;
	ft_dec_to_rgb(data->shader_band_color, data->rgb);
	while (++y < data->scr_y / 10 + 5)
	{
		i = -1;
		while (++i < (data->scr_x / 100 * 40 + 5) - y)
			ft_modif_image(data, x + i, y);
	}
	y = -1;
	ft_dec_to_rgb(data->band_color, data->rgb);
	while (++y < data->scr_y / 10)
	{
		i = -1;
		while (++i < (data->scr_x / 100 * 40) - y)
			ft_modif_image(data, x + i, y);
	}
}

void	ft_fill_pixel_back(t_fdf_params *data)
{
	int i;

	i = 0;
	ft_dec_to_rgb(data->back_color, data->rgb);
	while (i + 4 < data->scr_x * data->scr_y * 4)
	{
		data->str_image[i] = data->rgb[2];
		data->str_image[i + 1] = data->rgb[1];
		data->str_image[i + 2] = data->rgb[0];
		data->str_image[i + 3] = 0;
		i += 4;
	}
}

void	ft_image(t_fdf_params *data)
{
	int		x;
	int		y;

	x = 0;
	y = -1;
	ft_fill_pixel_back(data);
	ft_print_tab_on_screen(data);
	ft_fill_pixel_band(data);
	mlx_put_image_to_window(data->sync, data->win, data->image, 0, 0);
}
