/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   keyboard.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rlegendr <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/12 15:00:42 by rlegendr     #+#   ##    ##    #+#       */
/*   Updated: 2018/11/26 12:25:00 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

void	ft_key1(t_fdf_params *data, int key)
{
	if (key == 125)
		data->up -= 1;
	else if (key == 126)
		data->up += 1;
	else if (key == 13)
		data->pos_y -= 1;
	else if (key == 0)
		data->pos_x -= 1;
	else if (key == 1)
		data->pos_y += 1;
	else if (key == 2)
		data->pos_x += 1;
	else if (key == 45)
		data->ok_end_color = data->ok_end_color == 0 ? 1 : 0;
	else if (key == 17)
		data->segment_type = data->segment_type == 0 ? 1 : 0;
}

void	ft_key2(t_fdf_params *data, int key)
{
	if (key == 7)
		data->back_color = ft_change_color(data->back_col, data->back_color);
	else if (key == 8)
		data->bre_color = ft_change_color(data->color, data->bre_color);
	else if (key == 9)
		data->band_color = ft_change_color(data->color, data->band_color);
	else if (key == 11)
		data->shader_band_color = ft_change_color(data->color,
				data->shader_band_color);
	else if (key == 46 && data->ok_end_color == 1)
		data->end_color = ft_change_color(data->color, data->end_color);
	else if (key == 24 || key == 69)
	{
		data->pos_y -= 3;
		data->d += 3;
		data->b += 2;
	}
	else if (key == 27 || key == 78)
	{
		data->pos_y = data->d <= 3 ? data->pos_y : data->pos_y + 3;
		data->d = data->d <= 3 ? 3 : data->d - 3;
		data->b = data->b <= 2 ? 2 : data->b - 2;
	}
}

void	ft_key3(t_fdf_params *data, int key)
{
	if (key == 53)
		exit(0);
	else if (key == 15)
		ft_init_data(data);
	else if (key == 34 || key == 40)
	{
		mlx_destroy_image(data->sync, data->image);
		mlx_destroy_window(data->sync, data->win);
		if (key == 34)
			ft_bigger_res(data);
		else if (key == 40)
			ft_smaller_res(data);
		data->sync = mlx_init();
		data->win = mlx_new_window(data->sync, data->scr_x, data->scr_y, "FdF");
		data->begin = 0;
		ft_keyboard(36, (void*)data);
		ft_keyboard(15, (void*)data);
		mlx_hook(data->win, 2, 0, ft_keyboard, (void*)data);
		mlx_hook(data->win, 5, 0, ft_mouse, (void*)data);
		mlx_loop(data->sync);
	}
}

int		ft_verif_key(int key, t_fdf_params *data)
{
	int bpp;
	int s_l;
	int endian;

	if (key == 15 || key == 53 || key == 24 || key == 69 ||
			key == 27 || key == 78 || key == 126 || key == 125 ||
			key == 13 || key == 0 || key == 1 || key == 2 ||
			key == 7 || key == 8 || key == 45 || key == 46 ||
			key == 9 || key == 11 || key == 34 || key == 40 ||
			key == 17)
		return (1);
	else if (key == 36)
	{
		data->image = mlx_new_image(data->sync, data->scr_x, data->scr_y);
		data->str_image = mlx_get_data_addr(data->image, &bpp, &s_l, &endian);
		data->begin = 1;
		return (1);
	}
	return (0);
}

int		ft_keyboard(int key, void *param)
{
	t_fdf_params	*data;
	int				i;

	i = 0;
	data = (t_fdf_params*)param;
	if ((ft_verif_key(key, data) == 0))
		return (0);
	if (data->begin == 1)
	{
		ft_key1(data, key);
		ft_key2(data, key);
		ft_key3(data, key);
		ft_image(data);
		ft_back_string(data);
	}
	return (0);
}
