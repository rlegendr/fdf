/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strrev.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rlegendr <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/12 08:19:46 by rlegendr     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/12 11:50:04 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strrev(char *str)
{
	int		i;
	int		len;
	char	*str2;

	i = 0;
	len = ft_strlen(str) - 1;
	if (!(str2 = (char *)malloc(sizeof(char) * ft_strlen(str) + 1)))
		return (0);
	str2 = ft_strcpy(str2, str);
	while (i < len)
	{
		str[i] = str2[len];
		str[len] = str2[i];
		i++;
		len--;
	}
	free(str2);
	return (str);
}
