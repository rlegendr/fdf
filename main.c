/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   main.c                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rlegendr <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/08 09:36:54 by rlegendr     #+#   ##    ##    #+#       */
/*   Updated: 2018/11/22 15:41:34 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

char	***read_fdf(const int fd, char ***fdf)
{
	t_list			*list_line;
	t_list			*temp;
	char			*line;
	int				i;
	int				len_list;

	i = -1;
	list_line = NULL;
	while (get_next_line(fd, &line) == 1)
	{
		ft_lstbck(&list_line, line, ft_strlen(line) + 1);
		free(line);
	}
	temp = list_line;
	len_list = ft_lstlen(list_line);
	if (!(fdf = (char***)malloc(sizeof(char**) * (len_list + 1))))
		return (NULL);
	while (++i < len_list)
	{
		fdf[i] = ft_strsplit(list_line->content, ' ');
		list_line = list_line->next;
	}
	fdf[i] = NULL;
	ft_lst_totaldel(temp);
	return (fdf);
}

int		error(int fd, int i)
{
	if (i == 0)
	{
		ft_putstr("usage: ./fdf targetfile\n");
		return (0);
	}
	if (i == 1)
	{
		close(fd);
		ft_putstr("read error\n");
		return (0);
	}
	if (i == 2)
	{
		close(fd);
		ft_putstr("map error\n");
		return (0);
	}
	return (0);
}

int		ft_verif_tab_line(char ***fdf)
{
	int		i;
	int		j;
	int		ref;

	ref = 0;
	if (*fdf)
	{
		j = -1;
		while ((*fdf)[++j])
			ref++;
	}
	else
		return (0);
	i = -1;
	while (fdf[++i])
	{
		j = 0;
		while (fdf[i][j])
			j++;
		if (ref != j)
			return (0);
	}
	return (1);
}

int		check_error_fdf(int argc, char *file, char ****fdf)
{
	int				fd;

	fd = 0;
	if (argc != 2)
		return (error(fd, 0));
	if ((fd = open(file, O_RDONLY)) < 0)
		return (error(fd, 1));
	*fdf = NULL;
	if ((*fdf = read_fdf(fd, *fdf)) == NULL)
		return (error(fd, 2));
	if (ft_verif_tab_line(*fdf) == 0)
		return (error(fd, 2));
	close(fd);
	return (1);
}

int		main(int argc, char **argv)
{
	t_fdf_params	*data;

	if ((data = malloc(sizeof(t_fdf_params))) == NULL)
		return (0);
	if (check_error_fdf(argc, argv[1], &(data->fdf)) == 0)
		return (0);
	data->segment_type = 1;
	data->scr_x = 0;
	data->begin = 0;
	data->filename = argv[1];
	ft_bigger_res(data);
	ft_init_color(data->color, data->back_col);
	ft_init_data(data);
	data->sync = mlx_init();
	data->win = mlx_new_window(data->sync, data->scr_x, data->scr_y, "FdF");
	ft_keyboard(36, (void*)data);
	mlx_hook(data->win, 2, 0, ft_keyboard, (void*)data);
	mlx_hook(data->win, 5, 0, ft_mouse, (void*)data);
	mlx_loop(data->sync);
	return (0);
}
