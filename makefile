#******************************************************************************#
#                                                           LE - /             #
#                                                               /              #
#    Makefile                                         .::    .:/ .      .::    #
#                                                  +:+:+   +:    +:  +:+:+     #
#    By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+      #
#                                                  #+#   #+    #+    #+#       #
#    Created: 2018/10/03 09:13:06 by bjuarez      #+#   ##    ##    #+#        #
#    Updated: 2018/11/26 12:24:52 by rlegendr    ###    #+. /#+    ###.fr      #
#                                                          /                   #
#                                                         /                    #
#******************************************************************************#


NAME	= fdf
FLAGS 	= -Wall -Werror -Wextra
SRC		= mouse.c bresenham.c image.c keyboard.c main.c put_tab_on_screen.c string.c tool1.c tool2.c 
LIBFT	= ./libft/
LIBFT.A = ./libft/libft.a
GCC		= gcc
LBX		= ./minilibx_macos/
MLXH	= -I /minilibx_macos/mlx.h
MLXLB	= -framework OpenGL -framework AppKit
OBJ		= $(SRC:.c=.o)
MINILIBX= $(LBX)/libmlx.a

all: libs $(NAME)

$(NAME): $(OBJ) $(LIBFT.A)
	@ $(GCC) -o $(NAME) $(MLXH) $(OBJ) minilibx_macos/libmlx.a $(MLXLB)  libft/libft.a
	@ echo "\033[1;36mfdf			\033[1;32mOK\033[0;37m"

libs:
	@ (cd $(LIBFT) && $(MAKE))

%.o: %.c fdf.h
	@ $(GCC) $(FLAGS) -I. -c $(SRC)

clean:
	@ rm -f $(OBJ)
	@ rm -f *.gch
	@ rm -rf fdf.dSYM
	@ rm -f .DS_Store
	@ (cd $(LIBFT) && $(MAKE) $@)
	@ echo "\033[1;33mclean 	\033[1;36mfdf		\033[1;32mOK"

fclean: clean
	@ rm -f $(NAME)
	@ (cd $(LIBFT) && $(MAKE) $@)
	@ echo "\033[1;33mfclean 	\033[1;36mfdf		\033[1;32mOK"

re: fclean all libft

rm .PHONY: clean fclean all re
