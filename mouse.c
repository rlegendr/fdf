/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   mouse.c                                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rlegendr <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/22 11:45:49 by rlegendr     #+#   ##    ##    #+#       */
/*   Updated: 2018/11/22 12:41:15 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

void	ft_bre_mouse_init(t_fdf_params *data, t_bre *bre)
{
	data->m_save_x = data->m_x0;
	data->m_save_y = data->m_y0;
	bre->dx = ft_abs(data->m_x1 - data->m_x0);
	bre->dy = ft_abs(data->m_y1 - data->m_y0);
	bre->sx = data->m_x0 < data->m_x1 ? 1 : -1;
	bre->sy = data->m_y0 < data->m_y1 ? 1 : -1;
	bre->err = (bre->dx > bre->dy ? bre->dx : -(bre->dy)) / 2;
}

void	ft_mouse_init(t_fdf_params *data)
{
	data->m_x0 = 0;
	data->m_y0 = 0;
	data->m_x1 = 0;
	data->m_y1 = 0;
}

void	ft_mouse_move(t_fdf_params *data)
{
	data->m_x0 = data->m_x1;
	data->m_y0 = data->m_y1;
	data->m_x1 = 0;
	data->m_y1 = 0;
}

int		ft_mouse(int button, int x, int y, t_fdf_params *data)
{
	if (button == 1)
	{
		if (data->m_x0 == 0 && data->m_y0 == 0)
		{
			data->m_x0 = x;
			data->m_y0 = y;
		}
		else if (data->m_x1 == 0 && data->m_y1 == 0)
		{
			data->m_x1 = x;
			data->m_y1 = y;
		}
		if (data->m_x0 != 0 && data->m_y0 != 0 &&
			data->m_x1 != 0 && data->m_y1 != 0)
		{
			ft_dec_to_rgb(data->bre_color, data->rgb);
			bresenham_mouse(data);
			mlx_put_image_to_window(data->sync, data->win, data->image, 0, 0);
			ft_back_string(data);
			ft_mouse_move(data);
		}
	}
	else
		ft_mouse_init(data);
	return (0);
}
