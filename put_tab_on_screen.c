/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   put_tab_on_screen.c                              .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rlegendr <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/12 11:01:51 by rlegendr     #+#   ##    ##    #+#       */
/*   Updated: 2018/11/22 15:42:12 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

#include <stdio.h>

void	calc_segment_init(t_params *par, int *dx, int *dy)
{
	par->y0 = par->y0 < 1 ? 0 : par->y0;
	par->y1 = par->y1 < 1 ? 0 : par->y1;
	*dx = par->x0 < par->x1 ? 1 : -1;
	*dy = par->y0 < par->y1 ? 1 : -1;
	par->save_x = par->x0;
	par->save_y = par->y0;
}

void	calc_segment(t_fdf_params *data, t_params *par, int i, int j)
{
	float			coef;
	int				dx;
	int				dy;

	calc_segment_init(par, &dx, &dy);
	coef = (par->y1 - par->y0) / (par->x1 - par->x0);
	if (coef < (float)0)
		coef = (float)-1 * coef;
	if (data->ok_end_color == 1)
		ft_check_color(data, i, j);
	while (1)
	{
		if (par->save_x > 2 && par->save_y > 2 &&
				par->save_x < data->scr_x - 2 &&
				par->save_y < data->scr_y - 2)
			ft_modif_image(data, par->save_x, par->save_y);
		if ((int)par->save_x == (int)par->x1 &&
				((int)par->save_y == (int)par->y1 ||
				(int)par->save_y == (int)par->y1 - 1))
			break ;
		par->save_x = par->save_x + dx;
		par->save_y = par->save_y + dy * coef;
	}
}

void	ft_seg_type(t_fdf_params *data, t_params *par, int i, int j)
{
	if (data->segment_type == 1)
		bresenham(data, par, i, j);
	else if (data->segment_type == 0)
		calc_segment(data, par, i, j);
}

void	ft_print2(t_fdf_params *da, t_params *par, int i, int j)
{
	while (da->fdf[++i])
	{
		j = -1;
		while (da->fdf[i][++j])
		{
			par->x0 = da->s_x - i * da->d + j * da->d;
			par->y0 = da->s_y + i * da->b + j * da->b
				- (da->b * ft_atoi(da->fdf[i][j]) * da->up) / 10;
			if (da->fdf[i][j + 1] != NULL && (da->mv = 0) == 0)
			{
				par->x1 = da->s_x - i * da->d + (j + 1) * da->d;
				par->y1 = da->s_y + i * da->b + (j + 1) * da->b
					- (da->b * ft_atoi(da->fdf[i][j + 1]) * da->up) / 10;
				ft_seg_type(da, par, i, j);
			}
			if (da->fdf[i + 1] != NULL && (da->mv = 1) == 1)
			{
				par->x1 = da->s_x - i * da->d + (j - 1) * da->d;
				par->y1 = da->s_y + (i + 1) * da->b + j * da->b
					- (da->b * ft_atoi(da->fdf[i + 1][j]) * da->up) / 10;
				ft_seg_type(da, par, i, j);
			}
		}
	}
}

void	ft_print_tab_on_screen(t_fdf_params *data)
{
	int			i;
	int			j;
	t_params	*par;

	i = -1;
	j = -1;
	data->s_x = data->scr_x / 100 * data->pos_x;
	data->s_y = data->scr_y / 100 * data->pos_y;
	if ((par = malloc(sizeof(t_params))) == NULL)
		exit(0);
	if (data->ok_end_color == 0)
		ft_dec_to_rgb(data->bre_color, data->rgb);
	ft_print2(data, par, i, j);
	free(par);
}
