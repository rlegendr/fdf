/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   string.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rlegendr <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/12 09:22:37 by rlegendr     #+#   ##    ##    #+#       */
/*   Updated: 2018/11/22 14:55:33 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

void	ft_instruct_string(t_fdf_params *data)
{
	mlx_string_put(data->sync, data->win, data->scr_x / 100,
			data->scr_y / 100 * 4, 0x00000000, data->filename);
	mlx_string_put(data->sync, data->win, data->scr_x / 100 * 2,
			data->scr_y / 50 * 7, 0x00FFFFFF, "W / A / S / D : Move");
	mlx_string_put(data->sync, data->win, data->scr_x / 100 * 2,
			data->scr_y / 50 * 7 + 25, 0x00FFFFFF, "up / down : Contrast");
	mlx_string_put(data->sync, data->win, data->scr_x / 100 * 2,
			data->scr_y / 50 * 7 + 50, 0x00FFFFFF, "+ / - : Zoom");
	mlx_string_put(data->sync, data->win, data->scr_x / 100 * 2,
			data->scr_y / 50 * 7 + 75, 0x00FFFFFF, "I / K : move res");
	if (data->segment_type == 1)
		mlx_string_put(data->sync, data->win, data->scr_x / 100 * 2,
			data->scr_y / 50 * 7 + 100, 0x00FFFFFF, "T : Segment type : full");
	if (data->segment_type == 0)
		mlx_string_put(data->sync, data->win, data->scr_x / 100 * 2,
			data->scr_y / 50 * 7 + 100, 0x00FFFFFF, "T : Segment type : empty");
	mlx_string_put(data->sync, data->win, data->scr_x / 100 * 2,
			data->scr_y / 50 * 7 + 125, 0x00FFFFFF, "R : Reset");
	mlx_string_put(data->sync, data->win, data->scr_x / 100 * 2,
			data->scr_y / 50 * 7 + 150, 0x00FFFFFF, "ESC : Quit");
}

void	ft_color_string(t_fdf_params *data)
{
	mlx_string_put(data->sync, data->win, data->scr_x / 100 * 2,
			data->scr_y / 50 * 29, 0x00FFFFFF, "Color commands :");
	mlx_string_put(data->sync, data->win, data->scr_x / 100 * 2,
			data->scr_y / 50 * 29 + 20, 0x00FFFFFF, "  X : Back");
	mlx_string_put(data->sync, data->win, data->scr_x / 100 * 2,
			data->scr_y / 50 * 29 + 45, 0x00FFFFFF, "  C : FDF");
	mlx_string_put(data->sync, data->win, data->scr_x / 100 * 2,
			data->scr_y / 50 * 29 + 70, 0x00FFFFFF, "  V : Band");
	mlx_string_put(data->sync, data->win, data->scr_x / 100 * 2,
			data->scr_y / 50 * 29 + 95, 0x00FFFFFF, "  B : Shader");
	if (data->ok_end_color == 0)
		mlx_string_put(data->sync, data->win, data->scr_x / 100 * 2,
			data->scr_y / 50 * 29 + 120, 0x00FFFFFF, "  N : Relief cmd : OFF");
	else if (data->ok_end_color == 1)
		mlx_string_put(data->sync, data->win, data->scr_x / 100 * 2,
			data->scr_y / 50 * 29 + 120, 0x00FFFFFF, "  N : Relief cmd : ON");
	mlx_string_put(data->sync, data->win, data->scr_x / 100 * 2,
			data->scr_y / 50 * 29 + 145, 0x00FFFFFF, "    M : Relief");
}

void	ft_back_string(t_fdf_params *data)
{
	int		speed;

	speed = data->ok_end_color + data->segment_type;
	ft_instruct_string(data);
	ft_color_string(data);
	mlx_string_put(data->sync, data->win, data->scr_x / 100 * 80 - 20,
		data->scr_y / 100 * 4, 0x00FFFFFF, "speed : ");
	if (speed == 2)
		mlx_string_put(data->sync, data->win, data->scr_x / 100 * 80 + 55,
			data->scr_y / 100 * 4, 0x00DF0101, "slow");
	if (speed == 1)
		mlx_string_put(data->sync, data->win, data->scr_x / 100 * 80 + 55,
			data->scr_y / 100 * 4, 0x00D7DF01, "medium");
	if (speed == 0)
		mlx_string_put(data->sync, data->win, data->scr_x / 100 * 80 + 55,
			data->scr_y / 100 * 4, 0x003ADF00, "fast");
}

void	ft_bigger_res(t_fdf_params *data)
{
	if (data->scr_x == 0)
	{
		data->scr_x = 1920;
		data->scr_y = 1080;
	}
	else if (data->scr_x == 1920)
	{
		data->scr_x = 2560;
		data->scr_y = 1313;
	}
	else if (data->scr_x == 2560)
	{
		data->scr_x = 600;
		data->scr_y = 400;
	}
	else if (data->scr_x == 600)
	{
		data->scr_x = 1280;
		data->scr_y = 720;
	}
	else if (data->scr_x == 1280)
	{
		data->scr_x = 1920;
		data->scr_y = 1080;
	}
}

void	ft_smaller_res(t_fdf_params *data)
{
	if (data->scr_x == 1920)
	{
		data->scr_x = 1280;
		data->scr_y = 720;
	}
	else if (data->scr_x == 1280)
	{
		data->scr_x = 600;
		data->scr_y = 400;
	}
	else if (data->scr_x == 600)
	{
		data->scr_x = 2560;
		data->scr_y = 1313;
	}
	else if (data->scr_x == 2560)
	{
		data->scr_x = 1920;
		data->scr_y = 1080;
	}
}
