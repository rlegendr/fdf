/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   tool1.c                                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rlegendr <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/15 12:46:37 by rlegendr     #+#   ##    ##    #+#       */
/*   Updated: 2018/11/22 14:04:56 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

void	ft_init_data(t_fdf_params *data)
{
	int		ratio_zoom;

	ratio_zoom = ft_zoom_init(data->fdf);
	data->up = 1;
	data->pos_x = 45;
	data->pos_y = 30 - ratio_zoom;
	data->d = 30 - (3 * ratio_zoom / 2);
	data->b = 20 - (2 * ratio_zoom / 2);
	data->back_color = data->back_col[9];
	data->band_color = data->color[9];
	data->shader_band_color = data->color[8];
	data->bre_color = data->color[9];
	data->end_color = data->color[1];
	data->ok_end_color = 0;
	data->m_x0 = 0;
	data->m_x1 = 0;
	data->m_x1 = 0;
	data->m_y1 = 0;
}

void	ft_hex_to_rgb(char *hex_color, char rgb[3])
{
	int		i;
	int		j;
	int		pow;

	i = 5;
	j = -1;
	pow = 1;
	rgb[0] = 0;
	rgb[1] = 0;
	rgb[2] = 0;
	while (i >= 0)
	{
		pow = pow == 0 ? 1 : 0;
		if (i % 2 != 0)
			j++;
		if ((hex_color[i] >= 'A' && hex_color[i] <= 'F'))
			rgb[j] = rgb[j] + (hex_color[i] - 'A' + 10) * ft_power(16, pow);
		else
			rgb[j] = rgb[j] + (hex_color[i] - '0') * ft_power(16, pow);
		i--;
	}
}

char	*ft_dec_to_hex(char *hex_color, int color)
{
	int		i;
	int		rest;
	int		temp_color;

	i = 0;
	temp_color = color;
	if ((hex_color = (char*)malloc(7)) == NULL)
		exit(0);
	while (i < 6)
	{
		rest = temp_color % 16;
		hex_color[i++] = rest > 9 ? (rest - 10) + 'A' : rest + '0';
		temp_color = temp_color / 16;
	}
	hex_color[i] = '\0';
	return (ft_strrev(hex_color));
}

void	ft_modif_image(t_fdf_params *da, int x, int y)
{
	int		i;

	i = y * da->scr_x * 4 + x * 4;
	if (x <= da->scr_x - 2 && y <= da->scr_y - 2 &&
			i <= (da->scr_x * da->scr_y) * 4 - 4 &&
			x >= 2 && y >= 2)
	{
		da->str_image[i] = da->rgb[0];
		da->str_image[i + 1] = da->rgb[1];
		da->str_image[i + 2] = da->rgb[2];
		da->str_image[i + 3] = 0;
	}
}

void	ft_dec_to_rgb(int color, char *rgb)
{
	char	*hex_color;

	hex_color = NULL;
	hex_color = ft_dec_to_hex(hex_color, color);
	ft_hex_to_rgb(hex_color, rgb);
	free(hex_color);
}
