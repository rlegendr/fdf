/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   tool2.c                                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rlegendr <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/16 14:04:14 by rlegendr     #+#   ##    ##    #+#       */
/*   Updated: 2018/11/21 14:10:03 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

void	ft_init_color(int *color, int *back_col)
{
	color[0] = 0x00DF0101;
	color[1] = 0x00DF7401;
	color[2] = 0x00D7DF01;
	color[3] = 0x003ADF00;
	color[4] = 0x0001DFD7;
	color[5] = 0x00013ADF;
	color[6] = 0x00A901DB;
	color[7] = 0x00DF01A5;
	color[8] = 0x00A9A9A9;
	color[9] = 0x00FFFFFF;
	back_col[0] = 0x008A0808;
	back_col[1] = 0x008A4b08;
	back_col[2] = 0x00868A08;
	back_col[3] = 0x00298A08;
	back_col[4] = 0x00088A85;
	back_col[5] = 0x0008298A;
	back_col[6] = 0x008A0886;
	back_col[7] = 0x008A0868;
	back_col[8] = 0x00A9A9A9;
	back_col[9] = 0x00000000;
}

int		ft_change_color(int *color, int actual)
{
	int i;

	i = 0;
	while (color[i] != actual)
		i++;
	if (i == 9)
		return (color[0]);
	else
	{
		i++;
		return (color[i]);
	}
	return (0);
}

void	ft_print_triple_tab(char ***tab)
{
	int				i;
	int				j;

	i = -1;
	while (tab[++i])
	{
		j = -1;
		while (tab[i][++j])
		{
			if (tab[i][j + 1] == NULL)
				ft_putstr(tab[i][j]);
			else
			{
				ft_putstr(tab[i][j]);
				ft_putchar(' ');
			}
		}
		ft_putchar('\n');
	}
}

int		ft_scope_calc(int scope)
{
	if (scope >= 2 && scope < 5)
		return (scope * 3);
	if (scope > 4 && scope < 7)
		return (scope * 2 + 2);
	if (scope > 6 && scope < 15)
		return (scope + 4);
	if (scope % 2 == 1)
		scope++;
	if (scope == 0)
		return (1);
	if (scope > 20)
		return (18);
	return (scope);
}

int		ft_zoom_init(char ***fdf)
{
	int		len;
	int		hight;
	int		i;
	int		scope;

	i = -1;
	hight = 0;
	len = 0;
	while (fdf[0][++i])
		len++;
	i = -1;
	while (fdf[++i])
		hight++;
	scope = (len + hight) / 20;
	len = scope;
	while ((len = len / 10) != 0)
		scope += 2;
	scope = ft_scope_calc(scope);
	return (scope);
}
